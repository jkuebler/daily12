console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');


    var title = document.getElementById("song-input").value;
    console.log('Song you entered is ' + title);
    makeNetworkCallToSongAPI(title);

} // end of getFormInfo


function makeNetworkCallToSongAPI(title){

    var xhr = new XMLHttpRequest(); // 1 - creating request object

    var url = "https://itunes.apple.com/search?term=" + title + "&entity=musicTrack&limit=1 "
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateHTMLWithYear(xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

}


function updateHTMLWithYear(response_text){

    var response_json = JSON.parse(response_text);

    // update a label
    var label1 = document.getElementById("song-release-year");

    // see if label is null or not
    if(response_json['resultCount'] == 0){
        label1.innerHTML = 'Apologies, data on song not available';
    } else{

        // strip date and artist from response
        var year = (response_json['results'][0]['releaseDate']).substring(0,4)
        var month = (response_json['results'][0]['releaseDate']).substring(5,7)
        var day = (response_json['results'][0]['releaseDate']).substring(8, 10)
        var artist = response_json['results'][0]['artistName']

        // update html with year and artist
        label1.innerHTML =  "\"" + response_json['results'][0]['trackName'] + "\" " + "by " + artist + " was released on " + month + "/" +
        day + "/" + year + ':';

        // make call with date
        makeNetworkCallToDateAPI(month, day);
    }
} // end of updateHTMLWithYear

function makeNetworkCallToDateAPI(month, day){

    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://numbersapi.com/" + month + "/" + day + "/date";

    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateHTMLWithDateTrivia(month, day, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateHTMLWithDateTrivia(month, day, response_text){

    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" );

    var item_text = document.createTextNode('\n' + response_text); // creating new text
    label_item.appendChild(item_text); // adding something to button with appendChild()

    var response_div = document.getElementById("paragraph-1");
    response_div.appendChild(label_item);


} // end of updateHTMLWithDateTrivia
