import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://localhost:51081' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/reset/'
    MOVIES_URL = SITE_URL + '/movies/'

    def is_json(self, resp):
      try:
        json.loads(resp)
        return True
      except ValueError:
        return False

    def test_put_reset_index(self):
   
        # altered response
        altered = {"title": "Harry Potter", "genres": "Fantasy"}

        # put request to alter -> HERE WE EDIT TWO MOVIES TO MAKE SURE RESET_INDEX RESET EVERYTHING
        r3 = requests.put(self.MOVIES_URL + "82", data = json.dumps(altered))
        r4 = requests.put(self.MOVIES_URL + "83", data = json.dumps(altered))


        # do reset_index
        r = requests.put(self.RESET_URL)
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode())

        # make sure result is success
        self.assertEqual(resp['result'], 'success')

        # make a get request for mid 82
        r2 = requests.get(self.MOVIES_URL + "82")
        self.assertTrue(self.is_json(r2.content.decode('utf-8')))
        resp2 = json.loads(r2.content.decode())
        
        # make a get request for mid 83
        r5 = requests.get(self.MOVIES_URL + "83")
        self.assertTrue(self.is_json(r5.content.decode('utf-8')))
        resp3 = json.loads(r5.content.decode())


        # expected responses for two random movies
        m1 = {"title" : "Antonia's Line (Antonia) (1995)", "genres": "Drama"} # mid 82
        m2 = {"title" : "Once Upon a Time... When We Were Colored (1995)", "genres" : "Drama"} # mid 83

        # make sure each response is equal to expected responses
        self.assertEqual(resp2['title'], m1['title'])
        self.assertEqual(resp2['genres'], m1['genres'])
        self.assertEqual(resp3['title'], m2['title'])
        self.assertEqual(resp3['genres'], m2['genres'])



    def test_put_reset_key(self):

        # altered response
        altered = {"title": "Harry Potter", "genres": "Fantasy"}

        # put request to alter
        r3 = requests.put(self.MOVIES_URL + "81", data = json.dumps(altered))

        # do reset on specific mid
        r = requests.put(self.RESET_URL + "81")
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode())

        # make sure result is success
        self.assertEqual(resp['result'], 'success')

        # make a get request
        r2 = requests.get(self.MOVIES_URL + "81")
        self.assertTrue(self.is_json(r2.content.decode('utf-8')))
        resp2 = json.loads(r2.content.decode())

        # expected response
        m = {"title" : "Things to Do in Denver when You're Dead (1995)", "genres": "Crime|Drama|Romance"}

        # make sure response is equal to expected response
        self.assertEqual(resp2['title'], m['title'])
        self.assertEqual(resp2['genres'], m['genres'])



if __name__ == "__main__":
    unittest.main()
