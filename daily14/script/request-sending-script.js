console.log('page load - entered main.js for js-other api');

var sendButton = document.getElementById('send-button');
sendButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');


    // BELOW WE GET ALL OF THE VALUES PERTAINING TO FORM

    // PORT NUM
    var portNum = document.getElementById("input-port-number").value;

    // host
    var selindex = document.getElementById('select-server-address').selectedIndex;
    var url_base = document.getElementById('select-server-address').options[selindex].value;

    if (url_base == "localhost") {
        url_base = "http://localhost"
    }

    // REQUEST TYPE
    var requestType;
    if (document.getElementById('radio-get').checked){
        requestType = "GET";
    }
    else if (document.getElementById('radio-put').checked) {
        requestType = "PUT";
    }
    else if (document.getElementById('radio-post').checked){
        requestType = "POST";
    }
    else if (document.getElementById('radio-delete').checked) {
        requestType = "DELETE";
    }

    // OTHER VALUES
    var useKey = false;
    var useMessageBody = false;
    if (document.getElementById('checkbox-use-key').checked){
        useKey = true;
    }
    if (document.getElementById('checkbox-use-message').checked) {
        useMessageBody = true;
    }
    var key = document.getElementById("input-key").value;
    var messageBody = document.getElementById("text-message-body").value;




    makeNetworkCallToServer(portNum, url_base, requestType, key, useKey, useMessageBody, messageBody);

} // end of getFormInfo


function makeNetworkCallToServer(portNum, url_base, requestType, key, useKey, useMessageBody, messageBody){

    var xhr = new XMLHttpRequest(); // 1 - creating request object

    // change url based on key
    if (useKey)
    {
        url = url_base + ":" + portNum + "/" + "movies" + "/" + key; // include key

    }
    else
    {
        url = url_base + ":" + portNum + "/movies/"; // no key
    }

    xhr.open(requestType, url, true);

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);

        // TODO
        displayResponse(xhr.responseText, requestType, key);

    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    if (useMessageBody)
    {
        console.log(messageBody)
        xhr.send(messageBody) // include message body
    }
    else
    {
        xhr.send(null) // no message
    }

} // end of makeNetworkCallToServer

function displayResponse(responseText, requestType, useKey)
{
    console.log("display response to user")


    // get labels
    var label = document.getElementById("response-label");
    var label2 = document.getElementById("answer-label");

    responseJson = JSON.parse(responseText);

    // change second label
    if (requestType == "GET") {

        if (useKey && responseJson['result'] == "success")
        {

            // get movie info
            var title = responseJson['title'];
            var genre = responseJson['genres'];

            label2.innerHTML = title + " belongs to the genre: " + genre;
        }

        // either failed or was not a key-type request
        else {
            label2.innerHTML = "-";
        }

        label.innerHTML = responseText;
    }

    // not get request
    else {
        label2.innerHTML = "-";
    }


    // change 1st label pertaining to response
    label.innerHTML = responseText;

} // end of displayResponse
