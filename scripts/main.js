// respond to button click
console.log("Page load happened!")

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Entered get Form Info!")
    // get text from title, author and movie
    var title_text = document.getElementById('title-text').value;
    var actors_text = document.getElementById('actors-text').value;
    var selindex = document.getElementById('select-year').selectedIndex;
    var year = document.getElementById('select-year').options[selindex].value;
    console.log('title:' + title_text + ' actors: ' + actors_text + ' year ' + year);

    // get radio state
    var type = "Action"; // default
    if (document.getElementById('radio-romantic-comedy-value').checked){
            type = "Romantic Comedy";
    } else if (document.getElementById('radio-action-value').checked) {
            type = "Action";
    } else if (document.getElementById('radio-fantasy-value').checked) {
            type = "Fantasy";
    } else if (document.getElementById('radio-documentary-value').checked) {
            type = "Documentary";
    } else if (document.getElementById('radio-comedy-value').checked) {
            type = "Comedy";
    } else if (document.getElementById('radio-drama-value').checked) {
            type = "Drama";
    }

    // print  type
    console.log('type: ' + type);

    // get rating
    var selindex2 = document.getElementById('select-rating').selectedIndex;
    var rating = document.getElementById('select-rating').options[selindex2].value;
    console.log('rating: ' + rating)

    // make dictionary
    movie_dict = {};
    movie_dict['title'] = title_text;
    movie_dict['actors'] = actors_text;
    movie_dict['year'] = year;
    movie_dict['type'] = type;
    movie_dict['rating'] = rating;
    console.log(movie_dict);

    displaymovie(movie_dict);
}

function displaymovie(movie_dict){
    console.log('entered displaymovie!');
    console.log(movie_dict);
    // get fields from movie and display in label.
    var movie_top = document.getElementById('movie-top-line');
    movie_top.innerHTML = movie_dict['title'];

    var movie_body = document.getElementById('movie-body');
    movie_body.innerHTML = movie_dict['title'] + ' is a ' + movie_dict['type'] + ' film '
    + ' starring ' + movie_dict['actors'] + ' made in the year ' + movie_dict['year'] + ' and is rated '
    + movie_dict['rating'] + '/10';

}
